brokenlink-module
============================

Drupal module - BrokenLink module

Drupal Core: 7.x

CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation

Introduction
------------

Use this tool to check for Broken Links.

Installation
------------

Installation is quite simple:

Enable the module from admin/modules or use Drush:

drush pm-enable brokenlink
