<?php

/**
 * @file
 * module admin.
 */

function brokenlink_manager_settings() {
  $form = array();
  $form['brokenlink'] = array(
    '#type' => 'fieldset',
    '#title' => t('Scan links'),
  );
  $form['brokenlink']['scan'] = array(
    '#type' => 'submit',
    '#value' => t('Scan links'),
    '#submit' => array('brokenlink_scan_submit'),
  );
  $form['brokenlink_settings']['nodetypes'] = array(
    '#type' => 'fieldset',
    '#weight' => 1,
    '#title' => t('Scan scope'),
    '#description' => t('Apply scan broken link within following content types.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $node_types = node_type_get_types();
  $node_names = node_type_get_names();
  if (is_array($node_names) && count($node_names)) {
    foreach ($node_names as $key => $value) {
      $form['brokenlink_settings']['nodetypes']['brokenlink_scan_' . $node_types[$key]->type] = array(
        '#type' => 'checkbox',
        '#title' => t('@value (only affects content type @value)', array(
          '@value' => $value,
        )),
        '#default_value' => variable_get('brokenlink_scan_' . $node_types[$key]->type, FALSE),
      );
    }
  }
  return system_settings_form($form);
}

function brokenlink_scan_submit($form, &$form_state) {
  // Process batch.
  brokenlink_scan_run();
}
