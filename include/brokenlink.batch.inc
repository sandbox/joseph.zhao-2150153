<?php

/**
 * @file
 * Batch functions.
 */

/**
 * Runs the batch process for one entity.
 */
function _brokenlink_scan_batch_process_entity($id, &$context) {
  $node = node_load($id, NULL, TRUE);
  module_load_include('inc', 'brokenlink', 'include/brokenlink.handler');
  $links = _brokenlink_scan_exact_node_links($node);
  $links_array[$id] = array_values(array_unique($links));
  $links_num = count($links_array[$id]);
  if ($links_num > 0) {
    _brokenlink_scan_save_node_links($id, $links_array);
  }
  // Update batch progress information.
  if (!isset($context['results']['processed'])) {
    $context['results']['processed'] = 0;
  }
  $context['results']['processed']++;
  $context['message'] = t('We are processing node: @title and find @num links', array(
    '@title' => $node->title,
    '@num' => $links_num,
  ));
  unset($node, $links_num);
}

/**
 * Batch process finished callback.
 */
function _brokenlink_scan_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Scan finished with @nums nodes checked - BrokenLink.', array('@nums' => $results['processed']));
    $severity = WATCHDOG_INFO;
    $type = 'status';
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ));
    $severity = WATCHDOG_ERROR;
    $type = 'error';
  }
  drupal_set_message($message, $type);
  // Add watchdog to log the operation status.
  watchdog('BrokenLink', '%message', array('%message' => $message), $severity);
}
