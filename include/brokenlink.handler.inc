<?php

/**
 * @file
 * Handler helper.
 */

/**
 * Save links into database.
 */
function _brokenlink_scan_save_node_links($nid, $links_array) {
  // Rebuild node and links relationship.
  db_delete('brokenlink_node')->condition('nid', $nid)->execute();
  foreach ($links_array[$nid] as $uri) {
    $uri = ltrim($uri, '/');
    $link_external = _brokenlink_scan_check_link_type($uri);
    $link_hash = drupal_hash_base64($uri);
    $link = db_select('brokenlink_data', 'bld')
      ->fields('bld', array('link_id', 'uri'))
      ->condition('hash', $link_hash)
      ->execute()
      ->fetchAssoc();
    if (!$link) {
      $uri = urldecode(url($uri, array('absolute' => TRUE)));
      $link = array(
        'external' => $link_external === TRUE ? 1 : 0,
        'hash' => $link_hash,
        'uri' => $uri,
        'code' => '0',
        'message' => '',
        'checked_time' => 0,
        'created' => REQUEST_TIME,
      );
      drupal_write_record('brokenlink_data', $link);
    }
    // Check link status.
    module_load_include('inc', 'brokenlink', 'include/brokenlink.http');
    $link_status = _brokenlink_check_link($link['uri']);
    if (!is_null($link_status) && !empty($link_status)) {
      db_update('brokenlink_data')->fields(array(
        'code' => $link_status['code'],
        'message' => $link_status['message'],
        'checked_time' => REQUEST_TIME,
      ))->condition('link_id', $link['link_id'])
        ->execute();
    }
    // Save links and node relationship.
    $node_link = db_select('brokenlink_node', 'bln')
      ->fields('bln', array('nid'))
      ->condition('nid', $nid)
      ->condition('link_id', $link['link_id'])
      ->execute()
      ->fetchAssoc();
    if (!$node_link) {
      db_insert('brokenlink_node')
        ->fields(array(
          'nid' => $nid,
          'link_id' => $link['link_id'],
        ))
        ->execute();
    }
    unset($link_status, $link);
  }
}

/**
 * Return link is internel or external.
 */
function _brokenlink_scan_check_link_type($link) {
  return url_is_external($link);
}

/**
 * Exact link from node.
 */
function _brokenlink_scan_exact_node_links($node) {
  $languages = language_list();
  $url_options = empty($node->language) ? array('absolute' => TRUE) : array(
    'language' => $languages[$node->language],
    'absolute' => TRUE,
  );
  $path = url('node/' . $node->nid, $url_options);
  $resource = urldecode($path);

  if (empty($resource)) {
    return FALSE;
  }
  else {
    if (preg_match('|^http[s]?://|', $resource) == 0) {
      return FALSE;
    }
  }
  // QueryPath.
  $qp = htmlqp($resource);
  foreach ($qp->branch()->find('a') as $link_a) {
    $path = $link_a->attr('href');
    if ($path <> '' && $path <> '/' && $path <> '#') {
      $links[] = $path;
    }
  }
  unset($node, $path, $resource, $qp);
  return !empty($links) ? $links : FALSE;
}

/**
 * Prepare nids.
 */
function brokenlink_prepare_nids() {
  $node_types = node_type_get_types();
  $node_names = node_type_get_names();
  if (is_array($node_names) && count($node_names)) {
    foreach ($node_names as $key => $value) {
      $node_type_enabled = variable_get('brokenlink_scan_' . $node_types[$key]->type, FALSE);
      if ($node_type_enabled === 1) {
        $node_type[] = $key;
      }
    }
  }
  if (isset($node_type) && !empty($node_type)) {
    $result = db_select('node', 'n')
      ->fields('n', array('nid', 'type', 'status'))
      ->condition('type', $node_type, 'IN')
      ->condition('status', '1')
      ->execute();
    $record = $result->fetchCol();
    $nums = $result->rowCount();
    if ($nums > 0) {
      return $record;
    }
  }
}
