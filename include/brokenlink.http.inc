<?php

/**
 * @file
 * Handler helper.
 */

//equire_once '../library/vendor/autoload.php';
module_load_include('php', 'brokenlink', 'library/vendor/autoload');

// Load class.
use Guzzle\Http\Client;
use Guzzle\Http\Exception\CurlException;
use Guzzle\Http\Exception\BadResponseException;

function _brokenlink_check_link($link_uri) {
// Headers.
  $headers = array(
    'X-Header' => 'Decarboni.se',
  );
// Options.
  $options = array(
    'timeout' => 10,
    'exceptions' => FALSE,
    'allow_redirects' => FALSE,
  );
// Send request.
  try {
    $client = new Client();
    $response = $client->get($link_uri, $headers, $options)->send();
    $data = array(
      'code' => $response->getStatusCode(),
      'message' => $response->getReasonPhrase(),
    );
  } catch (CurlException $e) {
    $data = array(
      'code' => $e->getCode(),
      'message' => $e->getMessage(),
    );
  }

  if (isset($data)) {
    return $data;
  } else {
    return NULL;
  }
}
