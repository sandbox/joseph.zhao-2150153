<?php
require_once 'vendor/autoload.php';
// Load class.
use Guzzle\Http\Client;
use Guzzle\Http\Exception\CurlException;
use Guzzle\Http\Exception\BadResponseException;
// Headers.
$headers = array(
  'X-Header' => 'Decarboni.se',
);
// Options.
$options = array(
  'timeout' => 10,
  'exceptions' => FALSE,
  'allow_redirects' => FALSE,
);
// Send request.
try {
  $link_id = '1000';
  $client = new Client();
  $response = $client->get('http://www.globalccsinstitute.com/123', $headers, $options)->send();
  $data[$link_id] = array(
    'code' => $response->getStatusCode(),
    'message' => $response->getReasonPhrase(),
  );
} catch (CurlException $e) {
  $data[$link_id] = array(
    'code' => $e->getCode(),
    'message' => $e->getMessage(),
  );
}

if (isset($data)) {
  print_r($data);
}
